﻿using CEB.SharedSecurityServices.S3Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
//using CEB.MTM.DomainServices.Email.Concrete;
//using CEB.MTM.DomainServices.Email.Contract;

namespace NewUserWelcomeUtility
{
    public class EmailInfo
    {
        //public int Id { get; set; }
        public string Email { get; set; }      
    }
    class Program
    {
        #region Variables
        static SqlConnection _connection;
        //static string _pattern = @"[\w!#$%&'*+\-/=?\^_`{|}~]+(\.[\w!#$%&'*+\-/=?\^_`{|}~]+)*@((([\-\w]+\.)+[a-zA-Z]{2,4})|(([0-9]{1,3}\.){3}[0-9]{1,3}))";
        static string connectionString = "" + ConfigurationManager.AppSettings["DataModel"];
        static string EmailTemplate = "" + ConfigurationManager.AppSettings["EmailTemplate"];
        static string EmailSubject = "" + ConfigurationManager.AppSettings["Email_Subject"];        
        static int MaxUserCount = Int32.Parse("" + ConfigurationManager.AppSettings["MaxUserCount"]);
        static StreamWriter writer;
        static StreamWriter log;
        static bool WriteToConsole = ConfigurationManager.AppSettings["WriteToConsole"].ToUpper().Equals("YES");
        private static SessionManager _sessionManager = null;
        private static SessionManager SessionManager
        {
            get
            {
                if (_sessionManager == null)
                    _sessionManager = new SessionManager(ConfigurationManager.AppSettings["S3URL"], "TN", "S3");
                return _sessionManager;
            }
        }
        private static EmailService emailService = new EmailService();
        private static EmailValidator emailValidator = new EmailValidator();
        private static string _csvFilePath;
        #endregion

        static void Main(string[] args)
        {
            //initiate streamwriters
            string writerPath = ConfigurationManager.AppSettings["LogFile_EmailStatus"].Replace(".", string.Format("{0:_yyyy-MM-dd_hh-mm-ss-tt.}", DateTime.Now));
            string logPath = ConfigurationManager.AppSettings["LogFile_Log"].Replace(".", string.Format("{0:_yyyy-MM-dd_hh-mm-ss-tt.}", DateTime.Now));
            FileInfo writerfi = new FileInfo(Directory.GetCurrentDirectory() + writerPath);
            FileInfo logfi = new FileInfo(Directory.GetCurrentDirectory() + logPath);
            if (!writerfi.Directory.Exists)
                Directory.CreateDirectory(Path.GetDirectoryName(Directory.GetCurrentDirectory() + writerPath));
            if (!logfi.Directory.Exists)
                Directory.CreateDirectory(Path.GetDirectoryName(Directory.GetCurrentDirectory() + logPath));
            writer = new StreamWriter(Directory.GetCurrentDirectory() + writerPath);
            log = new StreamWriter(Directory.GetCurrentDirectory() + logPath);


           /* if (args.Count() == 0 || string.IsNullOrWhiteSpace(args[0]))
            {
                WriteLogLine("ERROR:  Filepath to CSV file not provided.  Invalid parameters.");
                System.Environment.Exit(-1);
            }
            else
                _csvFilePath = args[0];
            */
            _csvFilePath = "MailNotification.csv";

            writer.AutoFlush = true;
            log.AutoFlush = true;
            WriteLogLine("Started...");

            writer.WriteLine("UserLogins,ToEmail,Status,Message");

            GetandProcessEmails(0);
            writer.Flush();
            writer.Close();

            WriteLogLine("");
            WriteLogLine("Process Complete!");
        }

        private static void GetandProcessEmails(int startId)
        {
            try
            {
                //WriteLogLine("  Getting all customer emails from DB");
                //Dictionary<string, List<EmailInfo>> UserDataByEmail = GetUserEmailsFromDB();

                WriteLogLine("  Getting all customer emails from CSV file");
                Dictionary<string, List<EmailInfo>> UserDataByEmail = GetUserEmailsFromFile();


                WriteLogLine("  Sending emails in batches of " + MaxUserCount.ToString());
                //PROCESS EMAILS IN BATCHES
                int index = 0;
                int totalSentCount = 0;
                while (UserDataByEmail.Count > (index * MaxUserCount))
                {
                    var batchedUserData = UserDataByEmail.Skip(index * MaxUserCount).Take(MaxUserCount).ToDictionary(x => x.Key, x => x.Value);
                    ProcessEmails(batchedUserData);
                    index++;
                    totalSentCount += batchedUserData.Keys.Count();
                    WriteLog(string.Format("\r  {0}/{1} emails sent", totalSentCount.ToString(), UserDataByEmail.Keys.Count().ToString()));
                }

            }
            catch (Exception ex)
            {
                WriteLogLine("ERROR: " + ex.Message);
                WriteLogLine(ex.StackTrace);
            }
            finally
            {
                if (_connection != null)
                    _connection.Close();
            }
        } 

        private static Dictionary<string, List<EmailInfo>> GetUserEmailsFromFile()
        {
            Dictionary<string, List<EmailInfo>> UserDataByEmail = new Dictionary<string, List<EmailInfo>>();

            UserDataByEmail = File.ReadLines(_csvFilePath)                                  
                                  .Select(line => line.Split(','))
                                  .ToDictionary(line => line[0], line => 
                                      {
                                          List<EmailInfo> userData = new List<EmailInfo>();

                                         
                                                     userData.Add(new EmailInfo()
                                                         {
                                                             Email = line[0]                                                                                                                      
                                                         });
                                                

                                          return userData;
                                      });

            return UserDataByEmail;
        }

        private static void ProcessEmails(Dictionary<string, List<EmailInfo>> userData)
        {
            log.WriteLine("    Getting temp S3 password links");
            Dictionary<string, string> tmpPasswordLinks = GetPasswordLinks(userData.Keys.ToList<string>());

            log.WriteLine("    Sending Emails");
            foreach (KeyValuePair<string, List<EmailInfo>> record in userData)
            {
                EmailInfo info = record.Value.First();
                bool isEmailSent = true;
                string message = string.Empty;

                if (emailValidator.IsValidEmail(record.Key, out message))
                {
                    if (tmpPasswordLinks.ContainsKey(record.Key) && !string.IsNullOrEmpty(tmpPasswordLinks[record.Key]) && !string.IsNullOrWhiteSpace(tmpPasswordLinks[record.Key]))
                    {
                        string tmpPasswordLink = tmpPasswordLinks[record.Key].Replace("S3.Url=", string.Empty);

                        string emailBody = GetEmailBody( record.Key, tmpPasswordLink);

                        try
                        {
                            SendEmail(record.Key, emailBody);
                        }
                        catch (Exception ex)
                        {
                            isEmailSent = false;
                            message = "An error occurred while attempting to send the email";
                            WriteLogLine("ERROR: " + ex.Message);
                            WriteLogLine(string.Format("    Email={0};", record.Key));
                            WriteLogLine(ex.StackTrace);
                        }


                        WriteEmailStatus(string.Format("{0},{1},{2}", record.Key, isEmailSent.ToString(), message));
                    }
                    else
                    {
                        message = "Temp Password for " + record.Key + " was not created properly";
                        WriteLogLine("TMP PASSWORD ERROR: " + message);
                        WriteEmailStatus(string.Format("{0},{1},{2}", info.Email, "false", message));
                    }
                }
                else
                {
                    WriteLogLine("INVALID EMAIL: " + record.Key + ", With Error : " + message);
                    WriteEmailStatus(string.Format("{0},{1},{2}", info.Email, "false", "Invalid email: " + record.Key + " , With TN  Error : " + message));
                }
            }
            writer.Flush();
            log.Flush();
        }

        #region Helper Methods
        private static Dictionary<string, string> GetPasswordLinks(List<string> emails)
        {
            Dictionary<string, string> passwordLinks = SessionManager.GetTemporaryPasswords(emails)
                                                                        .Data
                                                                        .ToDictionary(x => x.Email, x => x.Link);
            return passwordLinks;
        }

        private static string GetEmailBody(string email, string tempPasswordLink)
        {
            return string.Format(EmailTemplate, tempPasswordLink);
        }

        private static void SendEmail(string email, string body)
        {
            /*MailMessage mailMessage = new MailMessage();
            mailMessage.From = new MailAddress(EmailFrom);
            mailMessage.Body = body;
            mailMessage.Subject = EmailSubject;
            mailMessage.To.Add(email);*/
            emailService.SendMail(EmailSubject, email, body);
        }

        private static void WriteLogLine(string message)
        {
            if (WriteToConsole)
                Console.WriteLine(message);
            log.WriteLine(message);
        }

        private static void WriteLog(string message)
        {
            if (WriteToConsole)
                Console.Write(message);
            log.WriteLine(message);
        }

        private static void WriteEmailStatus(string message)
        {
            //if (WriteToConsole)
            //    Console.WriteLine(message);
            writer.WriteLine(message);
        }
        #endregion
    }
}
