﻿using S3Common.Logging;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TNS3Migration.Entity;

namespace TNS3Migration
{
    public class TNDBHelper
    {
        private static LogHelper _logger = LogHelper.Create(typeof(TNDBHelper)); 

        #region Client
        public static List<TNClient> GetTNClients()
        {
            List<TNClient> clients = new List<TNClient>();
           
            DataTable dt = new DataTable();

            try
            {
                using (SqlConnection con = new SqlConnection(ConfigReader.GetTPConnectionString()))
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("TN_GetClientDetails", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@ClientId", SqlDbType.VarChar)).Value = -1;
                    IDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    dt.Load(dr);
                    dr.Close();
                    con.Close();
                }

                if (dt != null && dt.Rows.Count > 0)
                {
                    TNClient client = null;
                    foreach (DataRow row in dt.Rows)
                    {
                        client = new TNClient();
                        client.ClientID = Convert.ToInt32(row["ClientID"]);
                        client.ClientName = row["ClientName"].ToString();
                        client.ClientCode = row["ClientCode"].ToString();

                        clients.Add(client);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Failed - GetTNClients : " + ex.Message);
            }
            return clients;
        }

        public static void SetTNClientMigrationStatus(int JobId, TNClientMigrateEnum Status, string Content, string StatusMessage)
        {
            using (SqlConnection con = new SqlConnection(ConfigReader.GetTPConnectionString()))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("S3_SetClientMigrationStatus", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@JobId", SqlDbType.VarChar)).Value = JobId;
                cmd.Parameters.Add(new SqlParameter("@Status", SqlDbType.Int)).Value = Status;
                cmd.Parameters.Add(new SqlParameter("@Content", SqlDbType.VarChar)).Value = Content;
                cmd.Parameters.Add(new SqlParameter("@StatusMessage", SqlDbType.VarChar)).Value = StatusMessage;
                IDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                dr.Close();
                con.Close();
            }
        }

        public static TNClient GetNextClientToMigrate(TNClientMigrateEnum status, bool EnableSkippable = false, bool ResetSkippable = false)
        {
            TNClient client = null;

            DataTable dt = new DataTable();

            using (SqlConnection con = new SqlConnection(ConfigReader.GetTPConnectionString()))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("S3_GetNextClientMigrationJob", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@Status", SqlDbType.BigInt)).Value = status;
                cmd.Parameters.Add(new SqlParameter("@EnableSkippable", SqlDbType.Bit)).Value = EnableSkippable;
                cmd.Parameters.Add(new SqlParameter("@ResetSkippable", SqlDbType.Bit)).Value = ResetSkippable;
                IDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                dt.Load(dr);
                dr.Close();
                con.Close();
            }

            if (dt != null && dt.Rows.Count > 0)
            {
                client = new TNClient();

                client.JobID = Convert.ToInt32(dt.Rows[0]["JobId"]);
                client.ClientCode = dt.Rows[0]["ClientCode"].ToString();
                client.ClientID = Convert.ToInt32(dt.Rows[0]["ClientID"]);
                client.ClientName = dt.Rows[0]["ClientName"].ToString();
            }
            return client;
        }

        #endregion Client

        #region User
        public static TNMigrateUser GetTNUserDetails( string userName)
        {
            TNMigrateUser user = null;

            DataTable dt = new DataTable();

            try
            {
                using (SqlConnection con = new SqlConnection(ConfigReader.GetTPConnectionString()))
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("TN_GetUserDetails", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@UserName", SqlDbType.VarChar)).Value = userName;
                    IDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    dt.Load(dr);
                    dr.Close();
                    con.Close();
                }

                if (dt != null && dt.Rows.Count > 0)
                {
                    user = new TNMigrateUser();
                    user.ClientCode = dt.Rows[0]["ClientCode"].ToString();
                    user.ClientID = Convert.ToInt32(dt.Rows[0]["ClientID"]);
                    user.ClientName = dt.Rows[0]["ClientName"].ToString();
                    user.CreateDate = Convert.ToDateTime(dt.Rows[0]["CreateDate"]);
                    user.Email = dt.Rows[0]["Email"].ToString();
                    user.IsLockedOut = Convert.ToBoolean(dt.Rows[0]["IsLockedOut"]);
                    user.S3UserId = Convert.ToInt32(dt.Rows[0]["S3UserId"]);
                    user.UserID = dt.Rows[0]["UserID"].ToString();
                    user.UserName = dt.Rows[0]["UserName"].ToString();

                }
            }
            catch (Exception ex)
            {
                _logger.Error("Failed - GetTNUsers for UserName : " + userName + ", with ErrorMessage : "+ ex.Message  );
            }
            return user;
        }

        public static TNMigrateUser GetNextUserToMigrate(TNMigrateStatus status, bool EnableSkippable = false, bool ResetSkippable= false)
        {
            TNMigrateUser user = null;

            DataTable dt = new DataTable();

            using (SqlConnection con = new SqlConnection(ConfigReader.GetTPConnectionString()))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("S3_GetNextUserMigrationJob", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@Status", SqlDbType.BigInt)).Value = status;
                cmd.Parameters.Add(new SqlParameter("@EnableSkippable", SqlDbType.Bit)).Value = EnableSkippable;
                cmd.Parameters.Add(new SqlParameter("@ResetSkippable", SqlDbType.Bit)).Value = ResetSkippable;
                IDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                dt.Load(dr);
                dr.Close();
                con.Close();
            }

            if (dt != null && dt.Rows.Count > 0)
            {
                string userName = dt.Rows[0]["Email"].ToString();
                int jobID = Convert.ToInt32(dt.Rows[0]["JobId"]);

                user = GetTNUserDetails(userName);

                if (user != null)
                {
                    user.JobId = jobID;
                    user.IsValidUser = true;
                }
                else
                {
                    user = new TNMigrateUser();
                    user.UserName = userName;
                    user.JobId = jobID;
                    user.IsValidUser = false;
                }
            }
            return user;
        }

        public static TNMigrateUser GetNextUserMailNotificationJob(bool EnableSkippable = false, bool ResetSkippable = false)
        {
            TNMigrateUser user = null;

            DataTable dt = new DataTable();

            using (SqlConnection con = new SqlConnection(ConfigReader.GetTPConnectionString()))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("S3_GetNextUserMailNotificationJob", con);
                cmd.CommandType = CommandType.StoredProcedure;                
                cmd.Parameters.Add(new SqlParameter("@EnableSkippable", SqlDbType.Bit)).Value = EnableSkippable;
                cmd.Parameters.Add(new SqlParameter("@ResetSkippable", SqlDbType.Bit)).Value = ResetSkippable;
                IDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                dt.Load(dr);
                dr.Close();
                con.Close();
            }

            if (dt != null && dt.Rows.Count > 0)
            {
                user = new TNMigrateUser();
                user.UserName = dt.Rows[0]["Email"].ToString();
                user.JobId = Convert.ToInt32(dt.Rows[0]["JobId"]); 
                user.PasswordLink = Convert.ToString(dt.Rows[0]["PasswordLink"]);

            }
            return user;
        }
        public static void SetTNUserMigrationStatus(int JobId, TNMigrateStatus Status, string Content, string StatusMessage, string PasswordLink)
        {
            using (SqlConnection con = new SqlConnection(ConfigReader.GetTPConnectionString()))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("S3_SetUserMigrationStatus", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@JobId", SqlDbType.VarChar)).Value = JobId;
                cmd.Parameters.Add(new SqlParameter("@Status", SqlDbType.Int)).Value = Status;
                cmd.Parameters.Add(new SqlParameter("@Content", SqlDbType.VarChar)).Value = Content;
                cmd.Parameters.Add(new SqlParameter("@StatusMessage", SqlDbType.VarChar)).Value = StatusMessage;
                cmd.Parameters.Add(new SqlParameter("@PasswordLink", SqlDbType.VarChar)).Value = PasswordLink;
                IDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                dr.Close();
                con.Close();
            }
        }

        #endregion User
    }
}
