﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using TNS3Migration;

namespace NewUserWelcomeUtility
{
    public class EmailService
    {
        private static string _emailPwd = string.Empty;


        public EmailService()
        {
            _emailPwd = ConfigReader.GetQueryEmailPassword();

           // if (string.IsNullOrEmpty(_emailPwd)) throw new Exception("Failed to Get QueryEmailPassword");
        }

        public void SendMail(string subject, string email, string emailBody)
        {
            MailMessage mailMessage = new MailMessage();
            mailMessage.From = new MailAddress(ConfigReader.GetQueryEmail());
            mailMessage.To.Add(new MailAddress(email));

            if (System.Configuration.ConfigurationManager.AppSettings.AllKeys.LongCount(k => k == "AutoForwardEmailRecipients") > 0)
            {
                string[] addresses = ConfigReader.GetAutoForwardEmailRecipients().Split(new char[] { ',' });
                foreach (string eAddress in addresses)
                {
                    if (!string.IsNullOrEmpty(eAddress))
                        mailMessage.Bcc.Add(new MailAddress(eAddress));
                }
            }

            mailMessage.IsBodyHtml = true;

            mailMessage.Subject = subject;

            mailMessage.Body = emailBody;

            using (SmtpClient smtpClient = new SmtpClient())
            {
                smtpClient.UseDefaultCredentials = false;
                smtpClient.Credentials = new System.Net.NetworkCredential(ConfigReader.GetQueryEmail(), _emailPwd);
                smtpClient.Send(mailMessage);
                smtpClient.Dispose();
            }
        }
    }
}
