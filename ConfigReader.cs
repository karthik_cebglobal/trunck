﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TNS3Migration
{
    public class ConfigReader
    {
        public static int S3CALLDelayInterval()
        {
            string interval = System.Configuration.ConfigurationManager.AppSettings["S3CALLDelayInterval"].ToString();
            return Convert.ToInt32(interval);
        }

        public static bool IsMigrationTest()
        {
            string enabledStr = System.Configuration.ConfigurationManager.AppSettings["IsMigrationTestEnabled"].ToString();
            return Convert.ToBoolean(enabledStr);
        }

        public static List<string> GetMigrationUsersList()
        {
            string users = System.Configuration.ConfigurationManager.AppSettings["MigrationUsersList"].ToString();
            return users.Split(';').ToList(); ;

        }

        public static List<string> GetMigrationClientList()
        {
            string clients = System.Configuration.ConfigurationManager.AppSettings["MigrationClientList"].ToString();
            if (clients != null)
                clients = clients.ToLower();
            return clients.Split(';').ToList();
        }

        public static string GetTPConnectionString()
        {
            return System.Configuration.ConfigurationManager.ConnectionStrings["TalentPoolConnectionString"].ConnectionString;
        }

        public static int GetS3TNCompanyFeatureID()
        {
            string featureID = System.Configuration.ConfigurationManager.AppSettings["S3TNAccessFeatureID"].ToString();
            return Convert.ToInt32(featureID);
        }

        public static string GetS3TNAccessGroupCode()
        {
            return System.Configuration.ConfigurationManager.AppSettings["S3TNAccessGroupCode"].ToString();
        }

        public static string GetS3TNAccessRoleCode()
        {
            return System.Configuration.ConfigurationManager.AppSettings["S3TNAccessRoleCode"].ToString();
        }


        public static string GetAutoForwardEmailRecipients()
        {
            return System.Configuration.ConfigurationManager.AppSettings["AutoForwardEmailRecipients"].ToString();
        }
        public static string GetQueryEmail()
        {
            return System.Configuration.ConfigurationManager.AppSettings["queryEmailid"].ToString();
        }
        public static string GetQueryEmailPassword()
        {
            return System.Configuration.ConfigurationManager.AppSettings["queryEmailpassword"].ToString();
        }


    }
}
