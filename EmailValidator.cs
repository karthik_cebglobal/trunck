﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewUserWelcomeUtility
{
    public class EmailValidator
    {
        public bool IsValidEmail(string email, out string errMsg)
        {
            errMsg = "";
            bool isValidUser = false;

            if( string.IsNullOrEmpty( email))
                return isValidUser;

            try
            {
                TNS3Migration.Entity.TNMigrateUser user = TNS3Migration.TNDBHelper.GetTNUserDetails(email);


                if ( user == null || string.IsNullOrEmpty(user.UserName))
                {
                    errMsg = "Email Not Found in TN DB";
                }
                else if (user.ClientID < 1 || string.IsNullOrEmpty(user.ClientName))
                {
                    errMsg = "Invalid Email, Client Not Associated";
                }
                else
                {
                    isValidUser = true;
                }
            }
            catch (Exception ex)
            {
                errMsg = ex.Message;
            }
            return isValidUser;
        }
    }
}